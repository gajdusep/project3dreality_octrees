﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Project3DRealta
{   
    /// <summary>
    /// Empty tree - cube without any voxels inside or subtrees
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EmptyTree<T> : Octree<T>
    {        
        public EmptyTree() { }
    }

    /// <summary>
    /// Filled tree - a cube of single color, color is saved in type T
    /// </summary>
    /// <typeparam name="T">Voxel type</typeparam>
    public class FilledTree<T> : Octree<T>
    {
        public T Voxel { get; set; }

        public FilledTree(T voxel)
        {
            this.Voxel = voxel;
        }
    }

    /// <summary>
    /// Partial tree - it has 2^dimension subtrees, the subtrees can be of any classes inherited of Octree class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PartialTree<T> : Octree<T>
    {        
        public Octree<T>[] Subtrees { get; set; }

        public PartialTree(Octree<T>[] subtrees)
        {
            this.Subtrees = subtrees;
        }
    }

    /// <summary>
    /// Abstract class for Octree representation. The PartialTree, FilledTree and Emty tree are derived from Octree.
    /// All operations are static methods. They are NOT commutative!        
    /// </summary>
    /// <typeparam name="T">Voxel</typeparam>
    public abstract class Octree<T>
    {     
        /// <summary>
        /// Checks is two octrees are the same with inner structure.
        /// </summary>
        /// <param name="A">First octree</param>
        /// <param name="B">Second otree</param>
        /// <returns>true if equal, false elsewhere</returns>
        public static bool AreEqual(Octree<T> A, Octree<T> B)
        {
            if (A is EmptyTree<T> && B is EmptyTree<T>)
            {
                return true;
            }
            if (A is FilledTree<T> fa && B is FilledTree<T> fb)
            {
                return fa.Voxel.Equals(fb.Voxel);
            }
            if (A is PartialTree<T> pa && B is PartialTree<T> pb)
            {
                int n = pa.Subtrees.Length;                
                for (int i = 0; i < n; i++)
                {
                    if (!AreEqual(pa.Subtrees[i], pb.Subtrees[i]))
                    {
                        return false;
                    }
                }
                return true; // all partial are equal
            }
            return false;
        }

        /// <summary>
        /// Makes union of octrees A,B. Does NOT make deep copy - after change of A/B, the union octree won't make sense.
        /// In case of union of two voxels, the color of voxel in A is important - union is NOT commutative.
        /// </summary>
        /// <param name="A">First octree</param>
        /// <param name="B">Second octree</param>
        /// <returns>Union of A,B</returns>
        public static Octree<T> Union(Octree<T> A, Octree<T> B)
        {            
            if (A is FilledTree<T> fa) // A is full, we don't care about colors B
            {
                return fa;
            }            
            else if (A is EmptyTree<T>) // A is empty, we add B
            {
                return B;
            }
            else // A is partial, we have to divide and call recursively
            {
                var pa = A as PartialTree<T>;
                int n = pa.Subtrees.Length;
                if (B is EmptyTree<T>) // B is empty, just return A
                {
                    return A;
                }
                else if (B is PartialTree<T> pb) // B is partial, already divided into 8 parts
                {
                    var editedChildren = new Octree<T>[n];
                    for (int i = 0; i < n; i++)
                    {
                        editedChildren[i] = Union(pa.Subtrees[i], pb.Subtrees[i]);
                    }
                    return new PartialTree<T>(editedChildren);
                }
                else // B is full tree
                {
                    var BFilled = B as FilledTree<T>;
                    T v = BFilled.Voxel;

                    var editedChildren = new Octree<T>[n];
                    for (int i = 0; i < n; i++)
                    {
                        editedChildren[i] = Union(pa.Subtrees[i], new FilledTree<T>(v));
                    }

                    return new PartialTree<T>(editedChildren);
                }
            }            
        }

        /// <summary>
        /// Makes intersection of octrees A,B. Does NOT make deep copy - after change of A/B, the union octree won't make sense.
        /// In case of intersection of two voxels, the color of voxel in A is important - intersection is NOT commutative.
        /// </summary>
        /// <param name="A">First octree</param>
        /// <param name="B">Second octree</param>
        /// <returns>Intersection of A,B</returns>
        public static Octree<T> Intersection(Octree<T> A, Octree<T> B)
        {            
            if (A is EmptyTree<T> || B is EmptyTree<T>)
            {
                return new EmptyTree<T>();
            }
            else if (A is FilledTree<T> fa)
            {
                if (B is FilledTree<T>) // B is full
                {
                    return A;
                }
                else // B is partial, call recursively
                {
                    var pb = B as PartialTree<T>;
                    int n = pb.Subtrees.Length;
                    var editedChildren = new Octree<T>[n];
                    for (int i = 0; i < n; i++)
                    {
                        editedChildren[i] = Intersection(new FilledTree<T>(fa.Voxel), pb.Subtrees[i]);
                    }
                    return new PartialTree<T>(editedChildren);
                }
            }
            else // A is partial
            {
                var pa = A as PartialTree<T>;
                int n = pa.Subtrees.Length;

                if (B is FilledTree<T> bf) // B is full
                {                                        
                    var editedChildren = new Octree<T>[8];
                    for (int i = 0; i < n; i++)
                    {
                        editedChildren[i] = Intersection(pa.Subtrees[i], new FilledTree<T>(bf.Voxel));
                    }
                    return new PartialTree<T>(editedChildren);
                }
                else // B is partial
                {
                    var pb = B as PartialTree<T>;                    
                    var editedChildren = new Octree<T>[n];
                    for (int i = 0; i < n; i++)
                    {
                        editedChildren[i] = Intersection(pa.Subtrees[i], pb.Subtrees[i]);
                    }
                    return new PartialTree<T>(editedChildren);                    
                }
            }
                        
        }

        /// <summary>
        /// Makes subtraction of octrees A,B. Does NOT make deep copy - after change of A/B, the union octree won't make sense.        
        /// </summary>
        /// <param name="A">Minuend octree</param>
        /// <param name="B">Subtrahend octree</param>
        /// <returns>Difference octree</returns>
        public static Octree<T> Subtraction(Octree<T> A, Octree<T> B)
        {
            if (A is EmptyTree<T>) // A is empty, return empty
            {
                return A;
            }
            else if (B is EmptyTree<T>) // B is empty, return A unchanged
            {
                return A;
            }
            else if (B is FilledTree<T>) // B is filled, return empty, nothing remains
            {
                return new EmptyTree<T>();
            }
            // now, B must be partial
            else if (A is FilledTree<T> f) // A is filled
            {                                                                 
                var pb = B as PartialTree<T>;
                int n = pb.Subtrees.Length;
                var editedChildren = new Octree<T>[n];
                for (int i = 0; i < n; i++)
                {
                    editedChildren[i] = Subtraction(new FilledTree<T>(f.Voxel), pb.Subtrees[i]);
                }
                return new PartialTree<T>(editedChildren);
                
            }
            else // A is partial
            {
                var pa = A as PartialTree<T>;                               
                var pb = B as PartialTree<T>;
                int n = pb.Subtrees.Length;
                var editedChildren = new Octree<T>[n];
                for (int i = 0; i < n; i++)
                {
                    editedChildren[i] = Subtraction(pa.Subtrees[i], pb.Subtrees[i]);
                }
                return new PartialTree<T>(editedChildren);                
            }
        }
    }


    /// <summary>
    /// Factory for making 3D octrees.
    /// Contains methods for making octrees different ways.
    /// </summary>
    /// <typeparam name="T">Voxel type</typeparam>
    public class Octree3DFactory<T>
    {
        /* ------- Jagged array construction methods ------- */

        /// <summary>
        /// Makes Octree of given 3D scene given by jagged array of voxels. 
        /// All dimensions of jagged array must be equal, if not, throws Exception.
        /// </summary>        
        /// <param name="voxels">Jagged array of voxels</param>
        /// <returns>Octree</returns>
        public Octree<T> Jagged3DArrayToOctree(T[][][] voxels)
        {
            if (!CheckArraySize(voxels))
            {
                // there is some error in voxels size
                throw new ArgumentOutOfRangeException("The size of voxels array isn't right. The array must have size n*n*n, n=2^k");
            }
            return Jagged3DArrayToOctreeSizeChecked(voxels);
        }

        /// <summary>
        /// Makes Octree of given 3D scene given by jagged array of voxels.        
        /// </summary>
        /// <param name="voxels">Jagged array of voxels</param>
        /// <returns>Octree</returns>
        private Octree<T> Jagged3DArrayToOctreeSizeChecked(T[][][] voxels)
        {
            // we count with right dimensions of voxels array... 

            if (voxels.Length == 1 && voxels[0].Length == 1 && voxels[0][0].Length == 1)
            {
                if (voxels[0][0][0] == null)
                {
                    return new EmptyTree<T>();
                }
                return new FilledTree<T>(voxels[0][0][0]);
            }
            else
            {
                Octree<T>[] subTrees = new Octree<T>[8];
                // CutArray(xDim, yDim, zDim);
                subTrees[0] = Jagged3DArrayToOctree(CutArray(voxels, 0, 0, 0));
                subTrees[1] = Jagged3DArrayToOctree(CutArray(voxels, 1, 0, 0));
                subTrees[2] = Jagged3DArrayToOctree(CutArray(voxels, 0, 1, 0));
                subTrees[3] = Jagged3DArrayToOctree(CutArray(voxels, 1, 1, 0));
                subTrees[4] = Jagged3DArrayToOctree(CutArray(voxels, 0, 0, 1));
                subTrees[5] = Jagged3DArrayToOctree(CutArray(voxels, 1, 0, 1));
                subTrees[6] = Jagged3DArrayToOctree(CutArray(voxels, 0, 1, 1));                
                subTrees[7] = Jagged3DArrayToOctree(CutArray(voxels, 1, 1, 1));

                return DecideWhichOctree(subTrees);
            }
        }

        /// <summary>
        /// Checks if jagged array has right sizes. All sizes must equal = n = 2^k.
        /// </summary>
        /// <param name="voxels">jagged array to check</param>
        /// <returns>does it have right sizes?</returns>
        private bool CheckArraySize(T[][][] voxels)
        {
            int n = voxels.Length;
            if (n == 0) return false; // the cube must be at least of size 1
            if ((n & (n - 1)) != 0) return false; // check is n is power of two

            for (int i = 0; i < n; i++)
            {
                if (voxels[i].Length != n) return false; 
                for (int j = 0; j < n; j++)
                {
                    if (voxels[i][j].Length != n) return false;                    
                }
            }
            return true;
        }

        /// <summary>
        /// Helping method in jagged array processing. 
        /// </summary>
        /// <param name="voxels">array to divide</param>
        /// <param name="xDim">0/1, "left or right"</param>
        /// <param name="yDim">0/1, "down or up"</param>
        /// <param name="zDim">0/1, "front or behind"</param>
        /// <returns>Cut array</returns>
        private T[][][] CutArray(T[][][] voxels, int xDim, int yDim, int zDim)
        {
            // first, second, third = 0 or 1 --> it says this half we want to take
            if (!((xDim == 0 || xDim == 1) && (yDim == 0 || yDim == 1) && (zDim == 0 || zDim == 1)))
            {
                throw new ArgumentOutOfRangeException("xDim, yDim and zDim must be either 0 or 1");
            }

            int half = voxels.Length / 2;            
            var toReturn = voxels.Select(
                y => y.Select(
                    z => z.Skip(zDim * half).Take(half).ToArray()
                    ).Skip(yDim * half).Take(half).ToArray()
                ).Skip(xDim * half).Take(half).ToArray();
            
            return toReturn;
        }
        
        /// <summary>
        /// Gets octrees from recursion, checks if they're all the same and can be united into a big one
        /// </summary>
        /// <param name="subtrees"></param>
        /// <returns></returns>
        private Octree<T> DecideWhichOctree(Octree<T>[] subtrees)
        {            
            var empties = from tree in subtrees
                          where tree.GetType() == typeof(EmptyTree<T>)
                          select tree;
            var partials = from tree in subtrees
                           where tree.GetType() == typeof(PartialTree<T>)
                           select tree;

            // all are empty
            if (empties.ToArray().Length == subtrees.Length)
            {
                return new EmptyTree<T>();                
            }

            // some are empty -> return divided tree
            if (empties.ToArray().Length != 0 || partials.ToArray().Length != 0)
            {
                return new PartialTree<T>(subtrees);                
            }

            // went through the previous test -> all are fulls, but we also need to know, if it's the same color
            T val = ((FilledTree<T>)(subtrees.First())).Voxel;
            if (subtrees.All(x => ((FilledTree<T>)x).Voxel.Equals(val)))
            {
                return new FilledTree<T>(val);
            }

            // else: return divided tree again:
            return new PartialTree<T>(subtrees);            
        }


        /* ------- Indeces of voxels construction methods ------- */
        
        /// <summary>
        /// Returns cleaned octree of given octree - merges the clusters of voxels of the same color.
        /// </summary>
        /// <param name="octree">Octree to clean</param>
        /// <returns>Cleaned octree</returns>
        private Octree<T> GetCleanedOctree(Octree<T> octree)
        {
            if (octree == null) return new EmptyTree<T>();
            else if (octree is FilledTree<T> f) return f;
            else if (octree is EmptyTree<T> e) return e;
            else
            {
                PartialTree<T> p = octree as PartialTree<T>;
                for (int i = 0; i < p.Subtrees.Length; i++)
                {
                    p.Subtrees[i] = GetCleanedOctree(p.Subtrees[i]);
                }
                var pp = DecideWhichOctree(p.Subtrees);
                return pp;
            }
        }

        /// <summary>
        /// Makes Octree of given 3D scene given by list of ValueTuples of indeces of voxels.
        /// </summary>
        /// <param name="voxelsIndeces">List of ValueTuple - (x,y,z,voxel)</param>
        /// <returns>Octree</returns>
        public Octree<T> IndecesOfVoxelsToOctree(List<ValueTuple<int, int, int, T>> voxelsIndeces)
        {
            if (voxelsIndeces.Count == 0) { return new EmptyTree<T>(); }

            // check that there is not negative value
            int min = Math.Min(voxelsIndeces.Min(x => x.Item1),
                Math.Min(voxelsIndeces.Min(x => x.Item2), voxelsIndeces.Min(x => x.Item3)));
            if (min < 0) { throw new ArgumentOutOfRangeException("The indeces must be bigger of equal to 0"); }

            // get maximum of indeces
            int max = Math.Max(voxelsIndeces.Max(x => x.Item1),
                Math.Max(voxelsIndeces.Max(x => x.Item2), voxelsIndeces.Max(x => x.Item3)));

            // get the closest power of two (slow computation, but we compute it just once)            
            int size = (int)Math.Pow(2, Math.Ceiling(Math.Log(max+1) / Math.Log(2)));
            if (size == 1)
            {
                return new FilledTree<T>(voxelsIndeces[0].Item4);
            }

            // create new octree
            var toClean = new PartialTree<T>(new Octree<T>[8]);

            // add all voxels to octree
            for (int i = 0; i < voxelsIndeces.Count; i++)
            {
                int x = voxelsIndeces[i].Item1;
                int y = voxelsIndeces[i].Item2;
                int z = voxelsIndeces[i].Item3;

                int actSize = size; // size for iterative recursion
                int actIndex = 0;
                PartialTree<T> actParent = toClean; 
                
                while (actSize > 1)
                {
                    // decide which subtree we need to choose
                    bool xLeft = x < actSize / 2;
                    bool yLeft = y < actSize / 2;
                    bool zLeft = z < actSize / 2;
                    if (xLeft && yLeft && zLeft) actIndex = 0;
                    if (!xLeft && yLeft && zLeft) actIndex = 1;
                    if (xLeft && !yLeft && zLeft) actIndex = 2;
                    if (!xLeft && !yLeft && zLeft) actIndex = 3;
                    if (xLeft && yLeft && !zLeft) actIndex = 4;
                    if (!xLeft && yLeft && !zLeft) actIndex = 5;
                    if (xLeft && !yLeft && !zLeft) actIndex = 6;
                    if (!xLeft && !yLeft && !zLeft) actIndex = 7;

                    // the bottom of recursion
                    if (actSize == 2) break;
                    
                    // if the subtree wasn't divided yet, divide it...
                    if (actParent.Subtrees[actIndex] == null)
                        actParent.Subtrees[actIndex] = new PartialTree<T>(new Octree<T>[8]);
                    
                    // actualize the indeces and actual octree
                    x = xLeft ? x : x - actSize / 2;
                    y = yLeft ? y : y - actSize / 2;
                    z = zLeft ? z : z - actSize / 2;                                        
                    actParent = actParent.Subtrees[actIndex] as PartialTree<T>;
                    actSize /= 2;
                }
                
                actParent.Subtrees[actIndex] = new FilledTree<T>(voxelsIndeces[i].Item4);                
            }

            // clean the tree, go through all the branches and merge the voxels of same color
            return GetCleanedOctree(toClean);
        }


        /* ------- Multidimensional array construction methods ------- */

        /// <summary>
        /// Makes Octree of given 3D scene given by multidmensional array.
        /// </summary>
        /// <param name="voxelsMulti">Multidimensional array of voxels. All dimensions MUST BE THE SAME!</param>
        /// <returns>Octree</returns>
        public Octree<T> Multidimens3DArrayToOctree(T[,,] voxelsMulti)
        {
            int x = voxelsMulti.GetLength(0);
            int y = voxelsMulti.GetLength(1);
            int z = voxelsMulti.GetLength(2);
            if (x != y || x != z)
            {
                throw new ArgumentException("The multidimensional array must have all three dimensions the same");
            }
            int n = x;

            // transform multidimensional to jagged - no need to write the same code twice
            T[][][] voxels = new T[n][][];
            for (int i = 0; i < n; i++)
            {
                voxels[i] = new T[n][];
                for (int j = 0; j < n; j++)
                {
                    voxels[i][j] = new T[n];
                    for (int k = 0; k < n; k++)
                    {
                        voxels[i][j][k] = voxelsMulti[i, j, k];
                    }
                }
            }

            // call jagged array method, size was already checked
            return Jagged3DArrayToOctreeSizeChecked(voxels);
        }        
    }

    /// <summary>
    /// Voxel class. Contains R,G,B byte values.
    /// Overrides Equals method - voxels equal if the RGB values equal.
    /// </summary>
    public class Voxel
    {
        public byte R, G, B;

        public Voxel(byte R, byte G, byte B)
        {
            this.R = R;
            this.G = G;
            this.B = B;
        }
        
        public static bool operator ==(Voxel v1, Voxel v2)
        { 
            if (object.ReferenceEquals(v2, null))
            {
                return object.ReferenceEquals(v1, null);
            }

            return v1.Equals(v2);
        }

        public static bool operator !=(Voxel v1, Voxel v2)
        {
            return !(v1 == v2);
        }

        public override bool Equals(object obj)
        {
            Voxel v = (Voxel)obj;
            return v.B == this.B && v.G == this.G && v.R == this.R;
        }        
    }
}