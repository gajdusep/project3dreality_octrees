﻿using System;
using System.Collections.Generic;

namespace Project3DRealta
{
    /// <summary>
    /// Class with static methods for obtaining examples of Octrees of Voxel
    /// </summary>
    public class TestMethods
    {       
        private static List<ValueTuple<int,int,int,Voxel>> Indeces(int i)
        {
            if (i == 1) // Three cubes - red, green, blue
            {
                return new List<ValueTuple<int, int, int, Voxel>>
                {
                    (0, 0, 0, new Voxel(255, 0, 0)),
                    (7, 7, 7, new Voxel(0, 255, 0)),
                    (7, 0, 0, new Voxel(0, 0, 255))
                };
            }
            if (i == 2) // two red cubes
            {
                return new List<ValueTuple<int, int, int, Voxel>>
                {
                    (1, 1, 1, new Voxel(255, 0, 0)),
                    (7, 3, 4, new Voxel(255, 0, 0))
                };
            }
            if (i == 3) // one big red cube - on indeces 0,0,0
            {
                return new List<ValueTuple<int, int, int, Voxel>>
                {
                    (0, 0, 0, new Voxel(255, 0, 0))
                };
            }
            if (i == 4) // the same as i=3, but different way
            {
                return new List<(int, int, int, Voxel)>
                {
                    (0, 1, 0, new Voxel(255, 0, 0)),
                    (1, 0, 0, new Voxel(255, 0, 0)),
                    (0, 0, 0, new Voxel(255, 0, 0)),
                    (1, 1, 0, new Voxel(255, 0, 0)),
                    (0, 1, 1, new Voxel(255, 0, 0)),
                    (1, 0, 1, new Voxel(255, 0, 0)),
                    (0, 0, 1, new Voxel(255, 0, 0)),
                    (1, 1, 1, new Voxel(255, 0, 0))
                };
            }
            if (i == 5)
            {
                return new List<(int, int, int, Voxel)>
                {
                    (0, 1, 0, new Voxel(255, 0, 0)),
                    (1, 0, 0, new Voxel(255, 0, 0)),
                    (0, 0, 0, new Voxel(255, 0, 0)),
                    (1, 1, 0, new Voxel(255, 0, 0)),
                    (0, 1, 1, new Voxel(255, 0, 0)),
                    (1, 0, 1, new Voxel(255, 0, 0)),
                    (0, 0, 1, new Voxel(255, 0, 0)),
                    (1, 1, 1, new Voxel(255, 0, 0)),
                    (1, 1, 2, new Voxel(0, 0, 255)),
                    (3, 0, 0, new Voxel(0, 0, 255)),
                };
            }
            if (i == 6) // very big sphere
            {
                int n = 256;
                var l = new List<(int, int, int, Voxel)>();
                for (int ii = 0; ii < n; ii++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        for (int k = 0; k < n; k++)
                        {
                            int ii2 = (ii - n / 2) * (ii - n / 2);
                            int jj2 = (j - n / 2) * (j - n / 2);
                            int kk2 = (k - n / 2) * (k - n / 2);
                            if (Math.Sqrt(ii2 + jj2 + kk2) < 100)
                            {
                                l.Add((ii, j, k, new Voxel(255, 0, 10)));                                
                            }
                        }
                    }
                }
                return l;
            }

            throw new IndexOutOfRangeException("this number of test doesn't contain any method yet");
        }          

        private static Voxel[][][] Jagged(int testNo)
        {
            if (testNo == 1) // some strange shape, more random, stairs...
            {
                Random rnd = new Random();
                int n = 16;
                Voxel[,,] vv = new Voxel[n, n, n];
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 7; j++)
                    {
                        vv[i, j, i + j] = new Voxel(150, 60, 150);
                        vv[j, j, i + j] = new Voxel(30, 255, 0);
                    }
                }

                Voxel[][][] voxels = new Voxel[n][][];
                for (int i = 0; i < n; i++)
                {
                    voxels[i] = new Voxel[n][];
                    for (int j = 0; j < n; j++)
                    {
                        voxels[i][j] = new Voxel[n];
                        for (int k = 0; k < n; k++)
                        {
                            voxels[i][j][k] = vv[i, j, k];
                        }
                    }
                }
                return voxels;
            }
            if (testNo == 2 || testNo == 3) // red or purple ball, shifted 
            {
                int n = 32;
                Voxel[,,] vv = new Voxel[n, n, n];
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        for (int k = 0; k < n; k++)
                        {
                            int ii2 = (i - n / 2) * (i - n / 2);
                            int jj2 = (j - n / 2) * (j - n / 2);
                            int kk2 = (k - n / 2) * (k - n / 2);
                            if (Math.Sqrt(ii2 + jj2 + kk2) < 10)
                            {
                                if (testNo == 2)
                                {
                                    vv[i, j, k] = new Voxel(150, 50, 120);
                                }
                                if (testNo == 3)
                                {
                                    vv[i, j, k] = new Voxel(255, 0, 30);
                                }
                            }
                        }
                    }
                }

                Voxel[][][] voxels = new Voxel[n][][];
                for (int i = 0; i < n; i++)
                {
                    voxels[i] = new Voxel[n][];
                    for (int j = 0; j < n; j++)
                    {
                        voxels[i][j] = new Voxel[n];

                        if (testNo == 2)
                        {
                            for (int k = 7; k < n; k++)
                            {
                                voxels[i][j][k] = vv[i, j, k - 7];
                            }
                        }
                        if (testNo == 3)
                        {
                            for (int k = 0; k < n - 6; k++)
                            {
                                if (j > 2)
                                {
                                    voxels[i][j][k] = vv[i, j - 2, k + 6];
                                }
                            }
                        }
                    }
                }
                return voxels;
            }
            if (testNo == 4) // two red cubes
            {
                int n = 8;
                Voxel[,,] vv = new Voxel[n, n, n];
                vv[1, 1, 1] = new Voxel(255, 0, 0);
                vv[7, 3, 4] = new Voxel(255, 0, 0);

                Voxel[][][] voxels = new Voxel[n][][];
                for (int i = 0; i < n; i++)
                {
                    voxels[i] = new Voxel[n][];
                    for (int j = 0; j < n; j++)
                    {
                        voxels[i][j] = new Voxel[n];
                        for (int k = 0; k < n; k++)
                        {
                            voxels[i][j][k] = vv[i, j, k];
                        }
                    }
                }
                return voxels;
            }

            throw new IndexOutOfRangeException("this number of test doesn't contain any method yet");
        }          
                
        private static Voxel[,,] Multi(int i)
        {
            if (i == 1) // big red cube
            {
                Random rnd = new Random();
                int n = 8;
                Voxel[,,] vv = new Voxel[n, n, n];
                vv[0, 0, 0] = new Voxel(255, 0, 0);
                vv[7, 7, 7] = new Voxel(255, 0, 0);
                vv[7, 7, 6] = new Voxel(255, 0, 0);
                vv[7, 6, 7] = new Voxel(255, 0, 0);
                vv[6, 7, 7] = new Voxel(255, 0, 0);
                vv[6, 6, 7] = new Voxel(255, 0, 0);
                vv[7, 6, 6] = new Voxel(255, 0, 0);
                vv[6, 7, 6] = new Voxel(255, 0, 0);
                vv[6, 6, 6] = new Voxel(255, 0, 0);
                return vv;
            }
            if (i == 2) // two red cubes
            {
                int n = 8;
                Voxel[,,] vv = new Voxel[n, n, n];
                vv[1, 1, 1] = new Voxel(255, 0, 0);
                vv[7, 3, 4] = new Voxel(255, 0, 0);
                return vv;
            }
            if (i == 3) // exception case
            {
                int n = 1;                
                Voxel[,,] vv = new Voxel[n, n + 2, n];
                vv[0, 0, 0] = new Voxel(2, 1, 66);
                return vv;                
            }            

            throw new IndexOutOfRangeException("this number of test doesn't contain any method yet");
        }

        /// <summary>
        /// Returns example of Octree
        /// </summary>
        /// <param name="i">example number</param>  
        /// <returns>Example of octree</returns>
        public static Octree<Voxel> TestIndeces(int i)
        {
            var o = new Octree3DFactory<Voxel>();
            var l = Indeces(i);
            Console.WriteLine("Indeces example number {0} made.", i);
            return o.IndecesOfVoxelsToOctree(l);
        }

        /// <summary>
        /// Returns example of Octree
        /// </summary>
        /// <param name="i">example number</param>   
        /// <returns>Example of octree</returns>
        public static Octree<Voxel> TestJagged(int i)
        {
            var o = new Octree3DFactory<Voxel>();
            var j = Jagged(i);
            Console.WriteLine("Jagged array example number {0} made.", i);
            return o.Jagged3DArrayToOctree(j);
        }

        /// <summary>
        /// Returns example of Octree
        /// </summary>
        /// <param name="i">example number</param>   
        /// <returns>Example of octree</returns>
        public static Octree<Voxel> TestMulti(int i)
        {
            Octree3DFactory<Voxel> factory = new Octree3DFactory<Voxel>();
            var m = Multi(i);
            Octree<Voxel> o;
            try
            {
                o = factory.Multidimens3DArrayToOctree(m);
            }
            catch (Exception e)
            {                
                Console.WriteLine("{0}. Returning EmptyTree", e.Message);
                return new EmptyTree<Voxel>();
            }

            Console.WriteLine("Multiarray example number {0} made.", i);
            return o;
        }         
    }
}