﻿using System;
using Tao.FreeGlut;
using OpenGL;
using System.Linq;
using System.Collections.Generic;

/* THE OCTREE IMPLEMENTATION
 * Pavel Gajdusek
 *  - OctreeDataClasses.cs - contains Octree implementation
 *  - TestMethods.cs - contains examples of Octrees
 *  - Program.cs - contains rendering methods, uses opengl library
 * 
 * NOTICE:
 *   - the goal of this project is to implement 3D octree.
 *   - There are better algorithms for octree rendering,
 *     in this solution the triangle mesh is made from octrees. For more complex octrees the rendering might get slow.
 * 
 * REFERENCES: 
 * - opengl library: https://github.com/giawa/opengl4tutorials
 * - for linking all needed dlls watch the following tutorial: https://www.youtube.com/watch?v=2KEHrB82Z2M
*/

namespace Project3DRealta
{    
    /// <summary>
    /// Wrapper class for making all VBOs from an Octree
    /// </summary>
    public class BuffersWrapper
    {
        // buffers for the drawn object
        public VBO<Vector3> vertexBuffer;
        public VBO<Vector3> colors;
        public VBO<Vector3> normals;
        public VBO<int> triangles;
        
        // buffers for the lines (that will show the octree representation)
        public VBO<Vector3> linesBuffer;
        public VBO<Vector3> linesColors;
        public VBO<int> linesIndeces;

        // helping lists for the drawn object
        public List<Vector3> vertexList = new List<Vector3>();
        public List<Vector3> colorsList = new List<Vector3>();
        public List<Vector3> normalsList = new List<Vector3>();

        // helping lists for the lines (that will show the octree representation)
        public List<Vector3> linesBufferList = new List<Vector3>();
        public List<Vector3> linesColorsList = new List<Vector3>();

        /// <summary>        
        /// </summary>
        /// <param name="octree">Octree for rendering</param>
        /// <param name="w">Half of the size of the cube. </param>
        public BuffersWrapper(Octree<Voxel> octree, float w=3f)
        {
            // make all lists
            MakeOctreeArrays(octree, -w, -w, -w, 2 * w);     
            
            // make all VBOs from the lists
            vertexBuffer = new VBO<Vector3>(vertexList.ToArray());
            colors = new VBO<Vector3>(colorsList.ToArray());
            normals = new VBO<Vector3>(normalsList.ToArray());
            triangles = new VBO<int>(Enumerable.Range(0, vertexBuffer.Count + 1).ToArray(), BufferTarget.ElementArrayBuffer);
            
            // add the outer cube lines 
            linesBufferList.AddRange(new List<Vector3> {
                new Vector3(-w,-w,-w), new Vector3(-w,-w, w),
                new Vector3(-w,-w,-w), new Vector3(-w, w,-w),
                new Vector3(-w,-w,-w), new Vector3( w,-w,-w),
                new Vector3(w,w,w), new Vector3( w,-w, w),
                new Vector3(w,w,w), new Vector3( w, w,-w),
                new Vector3(w,w,w), new Vector3(-w, w, w),
                new Vector3(-w,w,w), new Vector3(-w,-w, w),
                new Vector3(-w,w,w), new Vector3(-w, w,-w),
                new Vector3(w,w,-w), new Vector3( w,-w,-w),
                new Vector3(w,w,-w), new Vector3(-w, w,-w),
                new Vector3(w,-w,w), new Vector3( w,-w,-w),
                new Vector3(w,-w,w), new Vector3(-w,-w, w),
            });
            for (int i = 0; i < 24; i++)
            {
                linesColorsList.Add(new Vector3(0, 0, 0));
            }
            
            // make all VBOs for lines
            linesBuffer = new VBO<Vector3>(linesBufferList.ToArray());
            linesIndeces = new VBO<int>(Enumerable.Range(0, linesBuffer.Count + 1).ToArray(), BufferTarget.ElementArrayBuffer);
            linesColors = new VBO<Vector3>(linesColorsList.ToArray());
        }

        /// <summary>
        /// Method for computing a normal of the triangle
        /// </summary>
        /// <returns>normal of the plane given by 3 points</returns>
        private static Vector3 ComputeNormal(Vector3 v1, Vector3 v2, Vector3 v3)
        {
            Vector3 v = v2 - v1;
            Vector3 u = v3 - v1;
            return new Vector3
            {
                X = u.Y * v.Z - u.Z * v.Y,
                Y = u.Z * v.X - u.X * v.Z,
                Z = u.X * v.Y - u.Y * v.X
            };
        }

        /// <summary>
        /// Adds all Vectors3 needed to show 1 cube to lists. 
        /// </summary>
        /// <param name="x">Beginning of cube in x coordinates</param>
        /// <param name="y">Beginning of cube in y coordinates</param>
        /// <param name="z">Beginning of cube in z coordinates</param>
        /// <param name="s">Size of cube</param>
        /// <param name="v">Color of the cube</param>
        public void MakeCubeArrays(float x, float y, float z, float s, Voxel v)
        {
            // add borders of cube
            var cubeVertices = new List<Vector3> {
                new Vector3(x, y, z), new Vector3(x+s, y, z), new Vector3(x, y+s, z),
                new Vector3(x+s, y+s, z), new Vector3(x, y+s, z), new Vector3(x+s, y, z),

                new Vector3(x+s, y+s, z), new Vector3(x, y+s, z+s), new Vector3(x, y+s, z),
                new Vector3(x+s, y+s, z), new Vector3(x+s, y+s, z+s), new Vector3(x, y+s, z+s),

                new Vector3(x+s, y+s, z), new Vector3(x+s, y, z), new Vector3(x+s, y+s, z+s),
                new Vector3(x+s, y, z+s), new Vector3(x+s, y+s, z+s), new Vector3(x+s, y, z),

                new Vector3(x, y, z), new Vector3(x, y, z+s), new Vector3(x+s, y, z),
                new Vector3(x+s, y, z+s), new Vector3(x+s, y, z), new Vector3(x, y, z+s),

                new Vector3(x, y, z), new Vector3(x, y, z+s), new Vector3(x, y+s, z),
                new Vector3(x, y+s, z+s), new Vector3(x, y+s, z), new Vector3(x, y, z+s),

                new Vector3(x, y, z+s), new Vector3(x+s, y, z+s), new Vector3(x, y+s, z+s),
                new Vector3(x+s, y+s, z+s), new Vector3(x, y+s, z+s), new Vector3(x+s, y, z+s),
            };

            // add colors of triangles
            var cubeColors = new List<Vector3>();
            for (int i = 0; i < 36; i++)
            {
                cubeColors.Add(new Vector3(v.R / 256f, v.G / 256f, v.B / 256f));
            }

            // add normals for good rendering
            var cubeNormals = new List<Vector3>();
            for (int i = 0; i < cubeVertices.Count; i += 3)
            {
                Vector3 n = ComputeNormal(cubeVertices[i], cubeVertices[i + 1], cubeVertices[i + 2]);
                for (int j = 0; j < 3; j++)
                {
                    cubeNormals.Add(n);
                }
            }

            vertexList.AddRange(cubeVertices);
            colorsList.AddRange(cubeColors);
            normalsList.AddRange(cubeNormals);
        }

        /// <summary>
        /// Adds all Vectors3 needed to show one octree to lists.
        /// </summary>
        /// <param name="octree"></param>
        /// <param name="x">Beginning of cube in x coordinates</param>
        /// <param name="y">Beginning of cube in y coordinates</param>
        /// <param name="z">Beginning of cube in z coordinates</param>
        /// <param name="size">Size of octree</param>
        public void MakeOctreeArrays(Octree<Voxel> octree, float x, float y, float z, float size)
        {
            linesBufferList.AddRange(new Vector3[] {
                new Vector3(x, y, z), new Vector3(x, y, z+size),
                new Vector3(x, y, z), new Vector3(x, y+size, z),
                new Vector3(x, y, z), new Vector3(x+size, y, z),

                new Vector3(x+size, y+size, z+size), new Vector3(x, y+size, z+size),
                new Vector3(x+size, y+size, z+size), new Vector3(x+size, y, z+size),
                new Vector3(x+size, y+size, z+size), new Vector3(x+size, y+size, z),

                new Vector3(x+size, y+size, z), new Vector3(x+size, y, z),
                new Vector3(x+size, y, z+size), new Vector3(x, y, z+size),
                new Vector3(x, y+size, z+size), new Vector3(x, y, z+size),

                new Vector3(x+size, y+size, z), new Vector3(x+size, y, z),
                new Vector3(x+size, y, z+size), new Vector3(x, y, z+size),
                new Vector3(x, y+size, z+size), new Vector3(x, y, z+size),
            });
            for (int i = 0; i < 24; i++)
            {
                linesColorsList.Add(new Vector3(0, 0, 0));                
            }
               
            if (octree is PartialTree<Voxel> partial)
            {
                MakeOctreeArrays(partial.Subtrees[0], x, y, z, size / 2);
                MakeOctreeArrays(partial.Subtrees[1], x + size / 2, y, z, size / 2);
                MakeOctreeArrays(partial.Subtrees[2], x, y + size / 2, z, size / 2);
                MakeOctreeArrays(partial.Subtrees[3], x + size / 2, y + size / 2, z, size / 2);
                MakeOctreeArrays(partial.Subtrees[4], x, y, z + size / 2, size / 2);
                MakeOctreeArrays(partial.Subtrees[5], x + size / 2, y, z + size / 2, size / 2);
                MakeOctreeArrays(partial.Subtrees[6], x, y + size / 2, z + size / 2, size / 2);
                MakeOctreeArrays(partial.Subtrees[7], x + size / 2, y + size / 2, z + size / 2, size / 2);

                float w = x;
                linesBufferList.AddRange(new Vector3[] {
                    new Vector3(x + size / 2, y + size / 2, z), new Vector3(x + size / 2, y + size / 2, z + size),
                    new Vector3(x, y + size / 2, z + size / 2), new Vector3(x + size, y + size / 2, z + size / 2),
                    new Vector3(x + size / 2, y, z + size / 2), new Vector3(x + size / 2, y + size, z + size / 2)
                });
            }
            else if (octree is FilledTree<Voxel> filled)
            {
                MakeCubeArrays(x, y, z, size, filled.Voxel);                
            }
            else if (octree is EmptyTree<Voxel> empty)
            {                
                // don't draw anything
            }            
        }
    }

    class Program
    {
        // buffers for the drawn object
        private static VBO<Vector3> vertexBuffer;
        private static VBO<Vector3> colors;
        private static VBO<Vector3> normals;
        private static VBO<int> triangles;

        // buffers for the lines (that will show the octree representation)
        private static VBO<Vector3> linesBuffer;
        private static VBO<Vector3> linesColors;
        private static VBO<int> linesIndeces;

        // width and height of the window
        private static int width = 800, height = 600;

        // the shader program
        private static ShaderProgram program;                

        // stopwatch useful for rotation of the object
        private static System.Diagnostics.Stopwatch watch;

        // variables for controling the movement of the 3D object
        private static float xAngle = 0f; //-0.3734f;
        private static float yAngle = 0f; //2.84520f;
        private static bool up = false, down = false, right = false, left = false;

        // show the  lines dividing cubes
        private static bool showLines = false;

        // buffers wrapper for all 3D objects
        private static BuffersWrapper buffersWrapper;                        

        /// <summary>
        /// Method with all testing examples. 
        /// Choose the root you want to see by editing the code.
        /// </summary>
        /// <returns>Example of octree</returns>
        private static Octree<Voxel> ChooseOctree()
        {
            Octree<Voxel> root;

            // get octree from indeces methods
            var threeCubes1 = TestMethods.TestIndeces(1);
            var twoRedCubes1 = TestMethods.TestIndeces(2);
            var oneBig1 = TestMethods.TestIndeces(3);
            var oneBig2 = TestMethods.TestIndeces(4);
            var docEx = TestMethods.TestIndeces(5);
            // var veryBigBall = TestMethods.TestIndeces(6); // this example will be very slow

            // get octree from jagged array methods
            var randomShape = TestMethods.TestJagged(1);
            var Ball1 = TestMethods.TestJagged(2);
            var Ball2 = TestMethods.TestJagged(3);
            var twoRedCubes2 = TestMethods.TestJagged(4);

            // get octree from multi array methods
            var oneBig3 = TestMethods.TestMulti(1);
            var twoRedCubes3 = TestMethods.TestMulti(2);            

            // check error methods
            var error1 = TestMethods.TestMulti(3);

            Console.WriteLine("------");

            // check that the same 3D scene gives the same octree by different methods            
            Console.Write("{0} ", Octree<Voxel>.AreEqual(twoRedCubes1, twoRedCubes2));            
            Console.WriteLine(Octree<Voxel>.AreEqual(twoRedCubes1, twoRedCubes3));            

            // check the set operations on octrees - best visible on balls
            var union = Octree<Voxel>.Union(Ball1, Ball2);
            var intersection1 = Octree<Voxel>.Intersection(Ball1, Ball2);
            var intersection2 = Octree<Voxel>.Intersection(Ball2, Ball1);
            var subtraction1 = Octree<Voxel>.Subtraction(Ball1, Ball2);
            var subtraction2 = Octree<Voxel>.Subtraction(Ball2, Ball1);

            // now asign some octree you want to see
            root = union; // EDIT HERE
            Console.WriteLine("------------");
            return root;
        }

        static void Main(string[] args)
        {
            var root = ChooseOctree();
            Console.WriteLine("THE CONTROLS:");
            Console.WriteLine("    ROTATE the cube: w, a, s, d");
            Console.WriteLine("    Show/hide the octree structure: l");


            // create an OpenGL window
            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);
            Glut.glutInitWindowSize(width, height);
            Glut.glutCreateWindow("OpenGL Tutorial");
            Glut.glutBitmapString(Glut.GLUT_BITMAP_HELVETICA_10, "neco");
            Glut.glutStrokeString(Glut.GLUT_STROKE_ROMAN, "neco");

            // provide the Glut callbacks
            Glut.glutIdleFunc(OnRenderFrame);
            Glut.glutDisplayFunc(OnDisplay);
            Glut.glutCloseFunc(OnClose);
            Glut.glutKeyboardFunc(OnKeyboardDown);
            Glut.glutKeyboardUpFunc(OnKeyboardUp);

            // enable depth testing to ensure correct z-ordering of our fragments
            Gl.Enable(EnableCap.DepthTest);
            
            // compile the shader program
            program = new ShaderProgram(VertexShader, FragmentShader);
            
            // set the view and projection matrix, which are static throughout this tutorial
            program.Use();
            program["projection_matrix"].SetValue(Matrix4.CreatePerspectiveFieldOfView(1.2f, (float)width / height, 0.1f, 1000f));
            program["view_matrix"].SetValue(Matrix4.LookAt(new Vector3(12, 0, 0), Vector3.Zero, new Vector3(0, 1, 0)));
            program["light_direction"].SetValue(new Vector3(1, 1, -1));

            float w = 3f;
            buffersWrapper = new BuffersWrapper(root, w);

            // create the objects we want to draw            
            // octree buffers
            vertexBuffer = buffersWrapper.vertexBuffer;
            colors = buffersWrapper.colors;
            normals = buffersWrapper.normals;
            triangles = buffersWrapper.triangles;

            // lines buffers
            linesBuffer = buffersWrapper.linesBuffer;
            linesColors = buffersWrapper.linesColors;
            linesIndeces = buffersWrapper.linesIndeces;
                        
            watch = System.Diagnostics.Stopwatch.StartNew();
            Glut.glutMainLoop();            
        }

        private static void OnKeyboardDown(byte key, int x, int y)
        {
            if (key == 'a') left = true;
            if (key == 'd') right = true;
            if (key == 'w') up = true;
            if (key == 's') down = true;
        }

        private static void OnKeyboardUp(byte key, int x, int y)
        {
            if (key == 'a') left = false;
            if (key == 'd') right = false;
            if (key == 'w') up = false;
            if (key == 's') down = false;
            if (key == 'l') showLines = !showLines;
        }

        private static void OnRenderFrame()
        {
            // calculate how much time has elapsed since the last frame
            watch.Stop();
            float deltaTime = (float)watch.ElapsedTicks / System.Diagnostics.Stopwatch.Frequency;
            watch.Restart();

            // use the deltaTime to adjust the angle of the cube and pyramid
            if (right) yAngle += deltaTime;
            if (left) yAngle -= deltaTime;
            if (up) xAngle += deltaTime;
            if (down) xAngle -= deltaTime;            

            // set up the OpenGL viewport and clear both the color and depth bits
            Gl.Viewport(0, 0, width, height);
            Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            Gl.ClearColor(1, 1, 1, 0);            

            // use our shader program
            Gl.UseProgram(program);
            
            // bind the vertex positions, colors and elements of the cube
            program["model_matrix"].SetValue(Matrix4.CreateRotationY(2*yAngle) * Matrix4.CreateRotationZ(2*xAngle) * Matrix4.CreateTranslation(new Vector3(1.5f, 0, 0)));

            // draw the cube
            Gl.BindBufferToShaderAttribute(vertexBuffer, program, "vertexPosition");
            Gl.BindBufferToShaderAttribute(normals, program, "vertexNormal");
            Gl.BindBufferToShaderAttribute(colors, program, "vertexColor");      
            Gl.BindBuffer(triangles);                                    
            Gl.DrawElements(BeginMode.Triangles, triangles.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
            
            // draw lines:             
            if (showLines)
            {
                Gl.BindBufferToShaderAttribute(linesBuffer, program, "vertexPosition");
                Gl.BindBufferToShaderAttribute(linesColors, program, "vertexColor");
                Gl.BindBuffer(linesIndeces);
                Gl.DrawElements(BeginMode.Lines, linesIndeces.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);             
            }

            Glut.glutSwapBuffers();
        }

        private static void OnClose()
        {
            // dispose of all of the resources that were created            
            vertexBuffer.Dispose();
            colors.Dispose();
            triangles.Dispose();
            normals.Dispose();
            linesBuffer.Dispose();
            linesColors.Dispose();
            linesIndeces.Dispose();
            program.DisposeChildren = true;
            program.Dispose();
        }

        // the vertex shader written in OpenGL shader language
        public static string VertexShader = @"
#version 130
in vec3 vertexPosition;
in vec3 vertexNormal;
in vec3 vertexColor;

out vec3 normal;
out vec3 color;

uniform mat4 projection_matrix;
uniform mat4 view_matrix;
uniform mat4 model_matrix;
void main(void)
{
    normal = normalize((model_matrix * vec4(vertexNormal, 0)).xyz);
    color = vertexColor;
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vertexPosition, 1);
}
";
        
        // the fragment shader written in OpenGL shader language
        public static string FragmentShader = @"
#version 130

uniform vec3 light_direction;

in vec3 normal;
in vec3 color;
out vec4 fragment;
void main(void)
{    
    float diffuse = max(dot(normal, light_direction), 0);
    float ambient = 0.3;
    float lighting = max(diffuse, ambient);    
    fragment = lighting * vec4(color, 1);    
}
";      

        private static void OnDisplay() { }
    }
}